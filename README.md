This will create a react app, using Visual Studio Code editor, in a docker environment.

Docker environment setup
=========================

1. Create .devcontainer folder and create devcontainer.json, Dockerfile as in the repository.
2. Install Remote-Containers extension in Visual Studio Code.
3. From Open a remote window section in Visual Studio Code, use Open Folder in Container option.

Technologies:
=============
* React
    Vite

* Styling - Tailwindcss

Setup React with Vite
======================

Create the client folder and navigate to it in command line.

1. Initialize the project with Vite
    npm init vite@latest
        Give a 
            project name - folder name f the project (can say current folder - this project - ./)
            package name - give a package name (this project - helloworld)
            select a framework (this project - react)
            select a variant (this project react)

2. Add packages

    npm install

3. Run the client side web application
    npm run dev
        
Setup Tailwindcss
==================

Follow instructions on the official website, create react app section - https://tailwindcss.com/docs/guides/create-react-app

1. Install all the necessary packages and dependencies
    npm install -D tailwindcss postcss autoprefixer

2. Initialize the project

    npx tailwindcss init -p

    This will create two config files
        * tailwind.config.js
        * postcss.config.js
         
3. Update Tailwond.configs.js with below configs

    module.exports = {
    content: ["./src/**/*.{html,js}"],
    theme: {
        extend: {},
    },
    plugins: [],
    }

4. Update src/index.css with below content (can remove existing default content)

    @tailwind base;
    @tailwind components;
    @tailwind utilities;

5. App.jsx update jsx return statement content with below content

        <h1 className="text-3xl font-bold underline">
        Hello world!
        </h1>

    * Entire App.jsx content would look like below 

        import { useState } from 'react'
        import logo from './logo.svg'
        import './App.css'

        function App() {
        const [count, setCount] = useState(0)

        return (
            <h1 className="text-3xl font-bold underline">
            Hello world!
            </h1>
        )
        }

        export default App

    * Refactor as below

        const App = () => {

        return (
            <h1 className="text-3xl font-bold underline">
            Hello world!
            </h1>
        )
        }

        export default App

    This is the initial react app. This will show "Hello world!" in an empty web page.